#Need to add pin to sleep stepper motor driver

import math
import time
from datetime import datetime
import ephem
import RPi.GPIO as GPIO
from decimal import *

GPIO.setwarnings(False) #Hides warnings when GPIO pins aren't "released"

#For calculating decimal numbers
getcontext().prec=6

#GPIO setup
GPIO.setmode(GPIO.BOARD)#Use board pin numbers, as in 1-26 
servo_pin = 12
step_pin = 18
dir_pin = 16
#GPIO 23
GPIO.setup(dir_pin, GPIO.OUT)
GPIO.output(dir_pin, False) #Set pin low
#GPIO 24
GPIO.setup(step_pin, GPIO.OUT)
GPIO.output(step_pin, False) #Set pin low
#GPIO 18
GPIO.setup(servo_pin,GPIO.OUT)
pwm=GPIO.PWM(servo_pin,50)
pwm.start(7.5)
#middle position, we want a DutyCycle of 7.5
pwm.ChangeDutyCycle(5)
#full right position, we want a duty cycle of 10

fr_steps=1600
steps_per_deg=fr_steps/360

#Calcs for servo
angle_zero = 5 #y1 duty cycle to point straig down
angle_max = 10 #y2 duty cycle to point straght up
m=(angle_max - angle_zero)/(Decimal(180)) # Slope of a line

print('Slope %4.5f deg' % (m)) 


#ISS tracking
degrees_per_radian = 180.0 / math.pi
home = ephem.Observer()
home.lon = '-6.2674937' # +W
home.lat = '53.3478'  # +N
home.elevation = 50     # meters
# Always get the latest ISS TLE data from:
# http://spaceflight.nasa.gov/realdata/sightings/SSapplications/Post/JavaSSOP/orbit/ISS/SVPOST.html
iss = ephem.readtle('ISS',
'1 25544U 98067A   16015.55124352  .00016717  00000-0  10270-3 0  9007',
'2 25544  51.6424 104.2216 0005982  18.8944 341.2430 15.54505728 21088'
)


while True:
    home.date = datetime.utcnow()
    iss.compute(home)
    azdeg_o=iss.az * degrees_per_radian
    altdeg_o=iss.alt * degrees_per_radian
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (altdeg_o, azdeg_o))
    #print('iss: altitude %4.1f deg' % (iss.alt * degrees_per_radian))
    #print('iss: azimuth %6.1f deg' % (iss.az * degrees_per_radian))
    time.sleep(2.0)
    #Let delay pass to get new calc

    home.date = datetime.utcnow() #These two lines are needed to recalculate location 
    iss.compute(home)
    azdeg_n = iss.az * degrees_per_radian
    altdeg_n = iss.alt * degrees_per_radian
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (altdeg_n, azdeg_n)) #Mew location


    
    #Servo control
    dutycyle_needed=m*Decimal(altdeg_n)+Decimal(angle_zero)
    print('Servo duty cycle %4.5f deg' % (dutycyle_needed)) 
    pwm.ChangeDutyCycle(dutycyle_needed)



#Use to set direction of stepper (changing step pin)
    #True and false needs to be checked 
    if azdeg_n < azdeg_o: #ccw
        print('New Az is smaller')
        GPIO.output(dir_pin, False) 
    elif azdeg_n == azdeg_o:
        print('They are the same')
    elif azdeg_n > azdeg_o: #cw
        print('New az is bigger')
        GPIO.output(dir_pin, True) 
    else:
        print('No idea')
    
    print('END')
    time.sleep(5.0)

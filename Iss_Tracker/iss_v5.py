import math
import time
from datetime import datetime
import ephem
import RPi.GPIO as GPIO


#GPIO setup
GPIO.setmode(GPIO.BOARD)#Use board pin numbers, as in 1-26 
servo_pin = 12
step_pin = 18
dir_pin = 16
#GPIO 23
GPIO.setup(dir_pin, GPIO.OUT)
GPIO.output(dir_pin, False) #Set pin low
#GPIO 24
GPIO.setup(step_pin, GPIO.OUT)
GPIO.output(step_pin, False) #Set pin low
#GPIO 18
GPIO.setup(servo_pin,GPIO.OUT)
pwm=GPIO.PWM(servo_pin,50)
pwm.start(7.5)
#middle position, we want a DutyCycle of 7.5
pwm.ChangeDutyCycle(5)
#full right position, we want a duty cycle of 10

GPIO.cleanup()

#ISS tracking
degrees_per_radian = 180.0 / math.pi
home = ephem.Observer()
home.lon = '-6.2674937' # +E
home.lat = '53.344104'  # +N
home.elevation = 50     # meters
# Always get the latest ISS TLE data from:
# http://spaceflight.nasa.gov/realdata/sightings/SSapplications/Post/JavaSSOP/orbit/ISS/SVPOST.html
iss = ephem.readtle('ISS',
'1 25544U 98067A   16012.14393245  .00016717  00000-0  10270-3 0  9042',
'2 25544  51.6431 121.2148 0006271   5.8278 354.2947 15.54399136 20559'
)


while True:
    home.date = datetime.utcnow()
    iss.compute(home)
    azdeg_o=iss.az * degrees_per_radian
    altdeg_o=iss.alt * degrees_per_radian
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (altdeg_o, azdeg_o))
    #print('iss: altitude %4.1f deg' % (iss.alt * degrees_per_radian))
    #print('iss: azimuth %6.1f deg' % (iss.az * degrees_per_radian))
    time.sleep(2.0)
    #Let delay pass to get new calc

    home.date = datetime.utcnow() #These two lines are needed to recalculate location 
    iss.compute(home)
    azdeg_n=iss.az * degrees_per_radian
    altdeg_n=iss.alt * degrees_per_radian
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (altdeg_n, azdeg_n)) #Mew location

    if azdeg_n < azdeg_o:
        print('New Az is smaller')
    elif azdeg_n == azdeg_o:
        print('They are the same')
    elif azdeg_n > azdeg_o:
        print('New az is bigger')
    else:
        print('No idea')
    time.sleep(2.0)

print('END')

## from http://www.thirdeyevis.com/pi-page-2.php
import RPi.GPIO as GPIO ## Import GPIO Library
import time ## Import 'time' library.  Allows us to use 'sleep'

GPIO.setmode(GPIO.BCM) ## Use BOARD pin numbering
GPIO.setup(4, GPIO.OUT) ## Setup GPIO pin 7 to OUT

## Define function named Blink()
def Blink(numTimes, speed):
    for i in range(0,numTimes): ## Run loop numTimes
        print "Iteration " + str(i+1) ##Print current loop
        GPIO.output(4, True) ## Turn on GPIO pin 7
        time.sleep(speed) ## Wait
        GPIO.output(4, False) ## Switch off GPIO pin 7
        time.sleep(speed) ## Wait
    print "Done" ## When loop is complete, print "Done"
    GPIO.cleanup()

## Prompt user for input
iterations = 10 ##number of times to blink
speed = 0.5 ##time

## Start Blink() function. Convert user input from strings to numeric data types and pass to Blink() as parameters
Blink(int(iterations),float(speed))

##more info https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins
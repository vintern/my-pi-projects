import math
import time
from datetime import datetime
import ephem
import RPi.GPIO as GPIO

servo_pin = 12
step_pin = 18
dir_pin = 16

#GPIO setup
GPIO.setmode(GPIO.BOARD)#Use board pin numbers, as in 1-26 
#GPIO 23
GPIO.setup(dir_pin, GPIO.OUT)
GPIO.output(dir_pin, False) #Set pin low
#GPIO 24
GPIO.setup(step_pin, GPIO.OUT)
GPIO.output(step_pin, False) #Set pin low
#GPIO 18
GPIO.setup(servo_pin,GPIO.OUT)
pwm=GPIO.PWM(servo_pin,50)
pwm.start(7.5)
#middle position, we want a DutyCycle of 7.5
pwm.ChangeDutyCycle(5)
#full right position, we want a duty cycle of 10

#GPIO.setup(12, GPIO.OUT)
#p = GPIO.PWM(12, 50)
#p.start(50)
#p.ChangeDutyCycle(90)
#p.stop() 
GPIO.cleanup()


degrees_per_radian = 180.0 / math.pi
 
home = ephem.Observer()
home.lon = '-6.2674937'   # +E
home.lat = '53.344104'      # +N
home.elevation = 50 # meters
# Always get the latest ISS TLE data from:
# http://spaceflight.nasa.gov/realdata/sightings/SSapplications/Post/JavaSSOP/orbit/ISS/SVPOST.html
iss = ephem.readtle('ISS',
'1 25544U 98067A   16012.14393245  .00016717  00000-0  10270-3 0  9042',
'2 25544  51.6431 121.2148 0006271   5.8278 354.2947 15.54399136 20559'
)

 
while True:
    home.date = datetime.utcnow()
    iss.compute(home)
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (iss.alt * degrees_per_radian, iss.az * degrees_per_radian))
    print('iss: altitude %4.1f deg' % (iss.alt * degrees_per_radian))
    print('iss: azimuth %6.1f deg' % (iss.az * degrees_per_radian))
    time.sleep(1.0)

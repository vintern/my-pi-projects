import math
import time
from datetime import datetime
import ephem
 
degrees_per_radian = 180.0 / math.pi
 
home = ephem.Observer()
home.lon = '-6.2674937'   # +E
home.lat = '53.344104'      # +N
home.elevation = 80 # meters
 
# Always get the latest ISS TLE data from:
# http://spaceflight.nasa.gov/realdata/sightings/SSapplications/Post/JavaSSOP/orbit/ISS/SVPOST.html
iss = ephem.readtle('ISS',
'1 25544U 98067A   16012.14393245  .00016717  00000-0  10270-3 0  9042',
'2 25544  51.6431 121.2148 0006271   5.8278 354.2947 15.54399136 20559'
)
 
while True:
    home.date = datetime.utcnow()
    iss.compute(home)
    print('iss: altitude %4.1f deg, azimuth %5.1f deg' % (iss.alt * degrees_per_radian, iss.az * degrees_per_radian))
    print('iss: altitude %4.1f deg' % (iss.alt * degrees_per_radian))
    print('iss: azimuth %6.1f deg' % (iss.az * degrees_per_radian))
    time.sleep(1.0)
